package ubs.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;


/**
 * Created by PABLO on 25/05/2016.
 */
public class Movie {
    private static final ArrayList<String> wordsToIgnore =
            new ArrayList<String>(Arrays.asList(new String[]{"da", "de", "do", "dos", "das", "em", "no", "nos",
            "na", "nas", "com", "sem", "a", "as", "o", "os", "aos", "ao", "at�", "por", "filme", "que",
            "suas", "sua", "seu", "seus", "e", "pela", "ele", "ela"}));

    private String nome;
    private int ano;
    private String pais;
    private ArrayList<String> generos;
    private String sinopse;
    private ArrayList<String> produtoras;
    private ArrayList<String> palavrasChave;
    private ArrayList<Person> elenco;
    private String poster;
    private String imdb_id;

    public Movie() {
        this.nome = "";
        this.ano = 1900;
        this.pais = "";
        this.generos = new ArrayList<>();
        this.sinopse = "";
        this.produtoras = new ArrayList<>();
        this.palavrasChave = new ArrayList<>();
        this.elenco = new ArrayList<>();
        this.poster = "noimage";
        this.setImdb_id("");
    }

    public Movie(String nome, int ano, String pais, ArrayList<String> generos,
                 String sinopse, ArrayList<String> produtoras, ArrayList<String> palavrasChave,
                 ArrayList<Person> elenco, String poster, String imdb_id) {
        this.nome = nome;
        this.ano = ano;
        this.pais = pais;
        this.generos = generos;
        this.sinopse = sinopse;
        this.produtoras = produtoras;
        this.palavrasChave = palavrasChave;
        this.elenco = elenco;
        this.poster = poster;
        this.setImdb_id(imdb_id);
    }

    public Movie(String nome, int ano, String pais, ArrayList<String> generos,
                 String sinopse, ArrayList<String> produtoras, ArrayList<String> palavrasChave,
                 ArrayList<Person> elenco, String imdb_id) {
        this.nome = nome;
        this.ano = ano;
        this.pais = pais;
        this.generos = generos;
        this.sinopse = sinopse;
        this.produtoras = produtoras;
        this.palavrasChave = palavrasChave;
        this.elenco = elenco;
        this.poster = "noimage";
        this.setImdb_id(imdb_id);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public ArrayList<String> getGeneros() {
        return generos;
    }

    public void setGeneros(ArrayList<String> generos) {
        this.generos = generos;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public ArrayList<String> getProdutoras() {
        return produtoras;
    }

    public void setProdutoras(ArrayList<String> produtoras) {
        this.produtoras = produtoras;
    }

    public ArrayList<String> getPalavrasChave() {
        return palavrasChave;
    }

    public void setPalavrasChave(ArrayList<String> palavrasChave) {
        this.palavrasChave = palavrasChave;
    }

    public ArrayList<Person> getElenco() {
        return elenco;
    }

    public void setElenco(ArrayList<Person> elenco) {
        this.elenco = elenco;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getImdb_id() {
		return imdb_id;
	}

	public void setImdb_id(String imdb_id) {
		this.imdb_id = imdb_id;
	}

	public ArrayList<String> getTags() {
        ArrayList<String> tags = new ArrayList<>();

        tags.add(Integer.toString(this.ano));
        tags.add(this.pais);
        tags.addAll(arrayStringFilter(generos));
        tags.addAll(arrayStringFilter(produtoras));
        tags.addAll(arrayStringFilter(palavrasChave));
        // Adiciona nome e sobrenome dos participantes
        for (Person p: elenco) {
            tags.addAll(arrayStringFilter(p.getNames()));
        }
        tags.addAll(tagGenerator(sinopse));
        tags.addAll(tagGenerator(nome));

        return tags;
    }

    /**
     * Separa a string em substrings e retira as palavras que devem ser ignoradas
     * na comparacao com a informacao do filme
     *
     * @param stringArray
     * @return Lista de palavras significativas
     */
    public ArrayList<String> tagGenerator(String stringArray) {
        String stringFiltered = stringFilter(stringArray);
        String[] stringSplitted = stringFiltered.split(" ");
        ArrayList<String> stringSplittedArray = new ArrayList<String>(Arrays.asList(stringSplitted));
        stringSplittedArray.removeAll(wordsToIgnore);

        return stringSplittedArray;
    }

    private String stringFilter(String entrada) {
    	if(entrada != null) {
    		String saida = entrada;

            saida = saida.replace(',', '\u0020');
            saida = saida.replace('.', '\u0020');
            saida = saida.replace(';', '\u0020');
            saida = saida.replace(':', '\u0020');
            saida = saida.replace('<', '\u0020');
            saida = saida.replace('>', '\u0020');
            saida = saida.replace('/', '\u0020');
            saida = saida.replace('?', '\u0020');
            saida = saida.replace('_', '\u0020');
            saida = saida.replace('-', '\u0020');
            saida = saida.replace('!', '\u0020');
            saida = saida.replace(')', '\u0020');
            saida = saida.replace('(', '\u0020');
            saida = saida.replace('�', 'a');
            saida = saida.replace('�', 'a');
            saida = saida.replace('�', 'a');
            saida = saida.replace('�', 'a');
            saida = saida.replace('�', 'e');
            saida = saida.replace('�', 'e');
            saida = saida.replace('�', 'e');
            saida = saida.replace('�', 'i');
            saida = saida.replace('�', 'o');
            saida = saida.replace('�', 'o');
            saida = saida.replace('�', 'o');
            saida = saida.replace('�', 'u');
            saida = saida.replace('�', 'u');
            saida = saida.replace('�', 'c');
            saida = saida.replace("  ", " ");
            saida = saida.trim();
            saida = saida.toLowerCase();

            return saida;
    	}
        return "";
    }

    public ArrayList<String> arrayStringFilter (ArrayList<String> entrada) {
        ArrayList<String> saida = new ArrayList<>();

        for(String s : entrada) {
            saida.add(stringFilter(s));
        }

        return saida;
    }

    public String getListOfStars () {
        String stars = "";

        for(Person p : getElenco()) {
            if(p.getFuncao().compareToIgnoreCase("ator") == 0) {
                stars += p.getNome() + "\n";
            }
        }

        return stars;
    }

    public String getListOfGenders () {
        String genders = "";

        for(String s : getGeneros()) {
            genders += s + "\n";
        }

        return genders;
    }

}
