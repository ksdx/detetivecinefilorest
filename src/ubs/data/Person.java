package ubs.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by PABLO on 25/05/2016.
 */
public class Person {
    private String nome;
    private String funcao;

    Person () {

    }

    public Person(String nome, String funcao) {
        this.nome = nome;
        this.funcao = funcao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public ArrayList<String> getNames() {
        String[] nameSplitted = nome.split(" ");

        return new ArrayList<String>(Arrays.asList(nameSplitted));
    }

}