package ubs.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import util.MovieFinder;
import util.StringProcessor;

public class DadosMovie {
	private List<Movie> movieList;
	
	public DadosMovie() {
		movieList = new ArrayList<Movie>();
		
		String line ="";
		String cvsSplitBy = ",";
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("ratings.csv")))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] lista = line.split(cvsSplitBy);
                
                Movie movie = new Movie();

                movie.setNome(lista[5].replace("\"", ""));
                movie.setAno(Integer.parseInt(lista[11].replace("\"", "")));
                
                String[] listGenres = lista[12].replace("\"", "").split("/");

                ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(listGenres));
                movie.setGeneros(stringList);

                ArrayList<Person> castCrew = new ArrayList<Person>();
                String[] listCastCrew = lista[7].replace("\"", "").split("/");
                for(int i = 0; i < listCastCrew.length; ++i) {
                	Person p = new Person(listCastCrew[i], "diretor");
                	castCrew.add(p);
                }
                
                movie.setElenco(castCrew);
                
                movie.setImdb_id(lista[1].replace("\"", ""));
                
                movie.setSinopse(lista[4]);
                
                movieList.add(movie);
              
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	/*
	public String getOverview(String imdb) {
		StringBuilder data = new StringBuilder();
		BufferedReader br = null;
		String url = 
				"http://api.themoviedb.org/3/find/"+imdb+"?api_key=fbcca0a07874b13df841b6d3c48e6780&external_source=imdb_id&language=pt";
		//&language=pt
		try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("GET");

            conn.connect();

            br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            if(br != null)
            	data.append(br.readLine());

            conn.disconnect();
            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
		
		String dt = data.toString();
		if(dt.startsWith("[")) {
			System.out.println(dt);
		}
		
		if(data.length() != 0) {
			return data.toString();
		}
		
		return "";
	}
	
	private String parseJSON(String s){
        try {
            JSONObject json = new JSONObject(s);
            JSONArray jsonArray = json.getJSONArray("movie_results");
            StringBuffer sb = new StringBuffer();
            if(jsonArray.length() > 0) {
            	JSONObject item = (JSONObject) jsonArray.get(0);
            	if(!item.isNull("overview"))
            		sb.append(StringProcessor.stringFilter(item.getString("overview")));
            	else
            		sb.append("");
            } else {
            	System.out.println("ERRO");
            }
            return sb.toString();
        } catch (Exception e) {
        	System.out.println(s);
            e.printStackTrace();
        }
        return null;
    }*/
	
	public List<Movie> getByGenre(String genre) {
		return movieList.stream().filter(m -> m.getGeneros().contains(genre)).collect(Collectors.toList());
	}
	
	public List<Movie> findMatchingMovies(String query) {
		String preProcessedQuery = StringProcessor.preProcessString(query);
		ArrayList<String> queryProcessed = StringProcessor.tagGenerator(preProcessedQuery);
		
		MovieFinder mf = new MovieFinder();
		mf.findMovie(queryProcessed, movieList);
		
		return mf.getListOfMovies();
	}
	
	public List<Movie> getMovieList() {
		return movieList;
	}
}
