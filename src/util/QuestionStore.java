package util;

import android.app.Application;

import java.util.ArrayList;

/**
 * Created by PABLO on 05/06/2016.
 */
public class QuestionStore extends Application {
    private ArrayList<String> questions = new ArrayList<>();

    public ArrayList<String> getQuestion() {
        return questions;
    }

    public void setQuestion(ArrayList<String> questions) {
        this.questions = questions;
    }

    public void addQuestion(String question) {
        questions.add(question);
    }

    public void removeQuestion(String question) {
        questions.remove(question);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
