package util;

import ubs.data.Movie;

/**
 * Created by PABLO on 27/05/2016.
 */
public class MovieAffinity {
    Movie m;
    int affinity;

    MovieAffinity() {
        m = new Movie();
        affinity = 0;
    }

    MovieAffinity(Movie m, int affinity) {
        this.m = m;
        this.affinity = affinity;
    }

    public Movie getMovie() {
        return m;
    }

    public void setMovie(Movie m) {
        this.m = m;
    }

    public int getAffinity() {
        return affinity;
    }

    public void setAffinity(int affinity) {
        this.affinity = affinity;
    }
}
