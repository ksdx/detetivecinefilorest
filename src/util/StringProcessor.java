package util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by PABLO on 25/05/2016.
 */
public class StringProcessor {
    private static final ArrayList<String> wordsToIgnore =
            new ArrayList<String>(Arrays.asList(new String[]{"da", "de", "do", "dos", "das", "em", "no", "nos",
                    "na", "nas", "com", "sem", "a", "as", "o", "os", "aos", "ao", "at�", "por", "filme", "que",
                    "suas", "sua", "seu", "seus", "e", "pela", "ele", "ela"}));

    /**
     * Separa a string em substrings e retira as palavras que devem ser ignoradas
     * na comparacao com a informacao do filme
     *
     * @param stringArray
     * @return Lista de palavras significativas
     */
    public static ArrayList<String> tagGenerator(String stringArray) {
        String stringFiltered = stringFilter(stringArray);
        String[] stringSplitted = stringFiltered.split(" ");
        ArrayList<String> stringSplittedArray = new ArrayList<String>(Arrays.asList(stringSplitted));
        stringSplittedArray.removeAll(wordsToIgnore);

        return stringSplittedArray;
    }

    /**
     * Substitui as pontua��es por espa�o em branco, retira a acentua��o das letras
     * e os espa�os em branco no come�o e fim da string e converte todas as letras em min�scula
     * @param entrada String a ser processada
     * @return String processada
     */
    public static String stringFilter(String entrada) {
        String saida = entrada;

        saida = saida.replace(',', '\u0020');
        saida = saida.replace('.', '\u0020');
        saida = saida.replace(';', '\u0020');
        saida = saida.replace(':', '\u0020');
        saida = saida.replace('<', '\u0020');
        saida = saida.replace('>', '\u0020');
        saida = saida.replace('/', '\u0020');
        saida = saida.replace('?', '\u0020');
        saida = saida.replace('_', '\u0020');
        saida = saida.replace('-', '\u0020');
        saida = saida.replace('!', '\u0020');
        saida = saida.replace(')', '\u0020');
        saida = saida.replace('(', '\u0020');
        saida = saida.replace('�', 'a');
        saida = saida.replace('�', 'a');
        saida = saida.replace('�', 'a');
        saida = saida.replace('�', 'a');
        saida = saida.replace('�', 'e');
        saida = saida.replace('�', 'e');
        saida = saida.replace('�', 'e');
        saida = saida.replace('�', 'i');
        saida = saida.replace('�', 'o');
        saida = saida.replace('�', 'o');
        saida = saida.replace('�', 'o');
        saida = saida.replace('�', 'u');
        saida = saida.replace('�', 'u');
        saida = saida.replace('�', 'c');
        saida = saida.replace("  ", " ");
        saida = saida.trim();
        saida = saida.toLowerCase();

        return saida;
    }
    
    public static String preProcessString(String query) {
    	return String.join(" ", query.split("%20"));
    }
}
