package util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import ubs.data.Movie;


/**
 * Classe que encontra os filmes que se encaixam melhor na consulta do usuario
 * na busca do usuario
 * Created by PABLO on 26/05/2016.
 */
public class MovieFinder {
    private ArrayList<MovieAffinity> moviesResult;

    public MovieFinder () {
        moviesResult = new ArrayList<>();
    }

    public ArrayList<MovieAffinity> getMoviesResult() {
        return moviesResult;
    }

    public void setMoviesResult(ArrayList<MovieAffinity> moviesResult) {
        this.moviesResult = moviesResult;
    }
    
    private String getPoster(String imdb) {
		StringBuilder data = new StringBuilder();
		BufferedReader br = null;
		String url = "http://api.themoviedb.org/3/movie/"+imdb+"/images?api_key=fbcca0a07874b13df841b6d3c48e6780&language=en";

		try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod("GET");

            conn.connect();

            br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            if(br != null)
            	data.append(br.readLine());

            conn.disconnect();
            br.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
		
		String dt = data.toString();
		if(dt.startsWith("[")) {
			System.out.println(dt);
		}
		
		if(data.length() != 0) {
			return data.toString();
		}
		
		return "";
	}
	
	private String parseImageJSON(String s) {
		try {
            JSONObject json = new JSONObject(s);
            JSONArray jsonArray = json.getJSONArray("posters");
            StringBuffer sb = new StringBuffer();
            if(jsonArray.length() > 0) {
            	JSONObject item = (JSONObject) jsonArray.get(0);
            	if(!item.isNull("file_path"))
            		sb.append(item.getString("file_path"));
            	else
            		sb.append("");
            } else {
            	System.out.println("ERRO");
            }
            return sb.toString();
        } catch (Exception e) {
        	System.out.println(s);
            e.printStackTrace();
        }
        return null;
	}
    
    public void movieMatch(Movie m, ArrayList<String> query) {
    	ArrayList<String> movieTags = new ArrayList<>();
    	int commonTags = 0; //tags em comum entre a query e o filme
        movieTags.clear();
        movieTags = m.getTags();
        for(String s : query) {
            if(movieTags.contains(s)) {
                commonTags += 1;
            }
        }
        if(commonTags > 0) {
        	m.setPoster(parseImageJSON(getPoster(m.getImdb_id())));
            MovieAffinity ma = new MovieAffinity(m, commonTags);
            if(moviesResult.isEmpty()) {
                moviesResult.add(ma);
            }
            else {
                int sizeMovieList = moviesResult.size();
                for(int index = 0; index < sizeMovieList; ++index) {
                    int aff = moviesResult.get(index).getAffinity();
                    if(ma.getAffinity() >= aff) {
                        moviesResult.add(index, ma);
                        index = sizeMovieList;
                    } else if(index == sizeMovieList - 1) {
                        moviesResult.add(ma);
                    }
                }
            }
        }
    }

    /**
     * Realiza a comparacao entre a consulta e a lista de filmes cadastrados em MovieStore
     * @param query consulta
     */
    public void findMovie(ArrayList<String> query, List<Movie> movieList) {
        if(movieList != null) {
        	for(Movie m : movieList) {
        		movieMatch(m, query);
        	}
        } else {
        	MovieStore movieStore = new MovieStore();
        	
        	for(Movie m : movieStore.getMoviesList()) {
        		movieMatch(m, query);
        	}
        }
    }

    public ArrayList<Movie> getListOfMovies() {
        ArrayList<Movie> listOfMovies = new ArrayList<>();

        for(MovieAffinity ma : moviesResult) {
            listOfMovies.add(ma.getMovie());
        }

        return listOfMovies;
    }
}
