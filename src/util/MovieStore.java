package util;

import java.util.ArrayList;
import java.util.Arrays;

import ubs.data.Movie;
import ubs.data.Person;

/** Classe que armazena os filmes
 * Created by PABLO on 27/05/2016.
 */
public class MovieStore {
    private ArrayList<Movie> moviesList;

    MovieStore() {
        moviesList = new ArrayList<>();

        ArrayList<String> generos = new ArrayList<>();
        ArrayList<String> produtoras = new ArrayList<>();
        ArrayList<String> palavrasChave = new ArrayList<>();
        ArrayList<Person> pessoas = new ArrayList<>();
        String sinopse;

        //Filme: As Duas Faces de Um Crime
        generos.addAll(Arrays.asList("crime", "drama", "misterio"));
        produtoras.addAll(Arrays.asList("Paramount"));
        palavrasChave.addAll(Arrays.asList("assassinato", "assassino", "advogado", "padre",
                "julgamento", "sangue", "bispo", "catolico", "cadeia", "delegacia", "policia", "tv",
                "coroinha", "foge", "fuga", "cena", "caso", "timido", "acusado", "americano"));
        sinopse = "Em Chicago, um arcebispo (Stanley Anderson) assassinado com 78 facadas. O crime choca a opiniao publica e tudo indica que o assassino um jovem de 19 anos (Edward Norton), que foi preso com as roupas cobertas de sangue da vitima. No entanto, um ex-promotor (Richard Gere) que se tornou um advogado bem-sucedido se propoe a defende-lo, sem cobrar honorarios, tendo um motivo para isto: adora ser coberto pela midia, alem de ter uma incrivel necessidade de vencer.";
        pessoas.add(new Person("Edward Norton", "Ator"));
        pessoas.add(new Person("Richard Gere", "Ator"));
        pessoas.add(new Person("Laura Linney", "Ator"));
        pessoas.add(new Person("John Mahoney", "Ator"));
        pessoas.add(new Person("Martin Vail", "Personagem"));
        pessoas.add(new Person("Janet Venable", "Personagem"));
        pessoas.add(new Person("Shaughnessy", "Personagem"));
        pessoas.add(new Person("Aaron", "Personagem"));

        Movie movie = new Movie("As Duas Faces de Um Crime", 1996, "EUA", generos, sinopse, produtoras,
                palavrasChave, pessoas, "primal_fear");

        moviesList.add(movie);

        generos = new ArrayList<>();
        produtoras = new ArrayList<>();
        palavrasChave = new ArrayList<>();
        pessoas = new ArrayList<>();

        //Filme: A Outra História Americana
        generos.addAll(Arrays.asList("crime", "drama"));
        produtoras.addAll(Arrays.asList("new line"));
        palavrasChave.addAll(Arrays.asList("prisao", "neo", "nazista", "irmao", "careca",
                "chuveiro", "negro", "gang", "gangue", "tiro", "assassinato", "cueca", "samba", "cancao",
                "skinhead", "loja", "quebra", "bastao", "professor", "escola", "cadeia", "racismo", "bombeiro",
                "vinganca", "violencia", "violento", "judeu", "tatuagem", "mein", "kampf", "redacao", "arma",
                "resumo", "sexo", "odio", "intolerancia", "redimir", "arrapendido", "arrepender", "tragedia",
                "americano"));
        sinopse = "Derek (Edward Norton) busca vazao para suas agruras tornando-se lider de uma gangue de racistas. A violencia o leva a um assassinato, e ele e condenado pelo crime. Tres anos mais tarde, ele sai da prisao e tem que convencer seu irmao (Edward Furlong), que esta prestes a assumir a lideranca do grupo, a nao trilhar o mesmo caminho.";
        pessoas.add(new Person("Edward Norton", "Ator"));
        pessoas.add(new Person("Edward Furlong", "Ator"));
        pessoas.add(new Person("Ethan Suplee", "Ator"));
        pessoas.add(new Person("Fairuza Balk", "Ator"));
        pessoas.add(new Person("Derek Vinyard", "Personagem"));
        pessoas.add(new Person("Danny", "Personagem"));
        pessoas.add(new Person("Stacey", "Personagem"));
        pessoas.add(new Person("Seth", "Personagem"));

        movie = new Movie("A Outra Historia Americana", 1998, "EUA", generos, sinopse, produtoras,
                palavrasChave, pessoas, "american_history_x");

        moviesList.add(movie);

        generos = new ArrayList<>();
        produtoras = new ArrayList<>();
        palavrasChave = new ArrayList<>();
        pessoas = new ArrayList<>();

        //Filme: Clube da Luta
        generos.addAll(Arrays.asList("drama"));
        produtoras.addAll(Arrays.asList("fox"));
        palavrasChave.addAll(Arrays.asList("surpresa", "final", "insonia", "transtorno", "dissociativo",
                "personalidade", "identidade", "multipla", "dupla", "violencia", "consumismo", "consumista", "testicular",
                "cancer", "grupo", "pinguim", "penis", "controle", "bar", "aviao", "queda", "explosao", "predio",
                "carro", "regra", "sociedade", "secreta", "mensagem", "subliminar", "sabao", "maleta", "sexo",
                "briga", "soco", "dente", "tiro", "boca", "bochecha", "arma", "neuronios", "cigarro", "fumante",
                "casa", "abandonada", "bicicleta", "chefe", "escritorio", "narrador"));
        sinopse = "Jack (Edward Norton) e um executivo jovem, trabalha como investigador de seguros, mora confortavelmente, mas ele esta ficando cada vez mais insatisfeito com sua vida mediocre. Para piorar ele esta enfrentando uma terrivel crise de insonia, ate que encontra uma cura inusitada para o sua falta de sono ao frequentar grupos de auto-ajuda. Nesses encontros ele passa a conviver com pessoas problematicas como a viciada Marla Singer (Helena Bonham Carter) e a conhecer estranhos como Tyler Durden (Brad Pitt). Misterioso e cheio de ideias, Tyler apresenta para Jack um grupo secreto que se encontra para extravasar suas angustias e tensoes atraves de violentos combates corporais";
        pessoas.add(new Person("Edward Norton", "Ator"));
        pessoas.add(new Person("Brad Pitt", "Ator"));
        pessoas.add(new Person("Helena Bonham Carter", "Ator"));
        pessoas.add(new Person("Jared Leto", "Ator"));
        pessoas.add(new Person("Tyler Durden", "Personagem"));
        pessoas.add(new Person("Marla Singer", "Personagem"));
        pessoas.add(new Person("Angel Face", "Personagem"));
        pessoas.add(new Person("David Fincher", "Diretor"));

        movie = new Movie("Clube da Luta", 1999, "EUA", generos, sinopse, produtoras,
                palavrasChave, pessoas, "clube_da_luta");

        moviesList.add(movie);

        generos = new ArrayList<>();
        produtoras = new ArrayList<>();
        palavrasChave = new ArrayList<>();
        pessoas = new ArrayList<>();

        //Filme: A Ultima Noite
        generos.addAll(Arrays.asList("drama"));
        produtoras.addAll(Arrays.asList("25th hour"));
        palavrasChave.addAll(Arrays.asList("droga", "trafico", "sofa", "festa", "racismo", "xenofobia",
                "espelho", "sonho", "bar", "carne", "pai", "namorada", "amigo", "briga", "soco", "cachorro",
                "clube", "interracial", "balanco", "namorado", "fuck","you", "reflexo", "onibnus", "carro",
                "monologo", "irlandes", "professor", "escola", "prisao", "2003"));
        sinopse = "Em Nova York, Montgomery \"Monty\" Brogan (Edward Norton) veio de uma familia da classe trabalhadora. Os melhores amigos de Monty, Jacob Elinsky (Philip Seymour Hoffman) e Francis Xavier Slaughtery (Barry Pepper) tiveram carreiras como professor de escola secundaria e corretor da bolsa, respectivamente, mas Monty pegou um rumo diferente e se tornou traficante de drogas. Isto o faz ganhar muito dinheiro, mas nao respeito. Um dia Monty esta em casa com Naturelle Riviera (Rosario Dawson), sua namorada que agora vive com ele, e chega a policia com um mandado de busca. Eles acham maconha rapidamente e Monty acaba pegando uma pena de sete anos. No seu ultimo dia antes de ir para a prisao, Monty programou se divertir com Jacob e Francis, alem da companhia de Naturelle. Porem Monty suspeita que ela o tenha entregado, pois ela sabia onde a droga estava escondida.";
        pessoas.add(new Person("Edward Norton", "ator"));
        pessoas.add(new Person("Philip Seymour Hoffman", "Ator"));
        pessoas.add(new Person("Barry Pepper", "Ator"));
        pessoas.add(new Person("Rosario Dawson", "Ator"));
        pessoas.add(new Person("Monty Montgomery Brogan", "Personagem"));
        pessoas.add(new Person("Jakob Elinsky", "Personagem"));
        pessoas.add(new Person("Naturelle Rivera", "Personagem"));
        pessoas.add(new Person("Francis Xavier Slaughtery", "Personagem"));

        movie = new Movie("A Ultima Noite", 2002, "EUA", generos, sinopse, produtoras,
                palavrasChave, pessoas, "a_ultima_noite");

        moviesList.add(movie);
    }

    public ArrayList<Movie> getMoviesList() {
        return moviesList;
    }

    public ArrayList<Movie> searchMoviesByTitle(String title) {
        ArrayList<Movie> result = new ArrayList<>();
        for(Movie m : moviesList) {
            if(m.getNome().contains(title)) {
                result.add(m);
            }
        }
        return result;
    }

}
