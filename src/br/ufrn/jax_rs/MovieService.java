package br.ufrn.jax_rs;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import ubs.data.DadosMovie;
import ubs.data.Movie;

@Path("/moviefinder")
public class MovieService {
	private DadosMovie dados = new DadosMovie();
	
	@GET
	@Produces("application/json")
	@Consumes("application/json")
	@Path("/buscarfilme/{query}")
	public List<Movie> getMatchingMovies(@PathParam("query") String query) {
		return dados.findMatchingMovies(query);
	}
}
